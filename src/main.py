#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from popaccount import *
from ui import Ui_MainWindow
from messagesmodel import *
import sys

settings = {
        'updateTimeout': 60000
        }
accounts = []

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.splitter.setStretchFactor(0, 0)
        self.ui.splitter.setStretchFactor(1, 1)
        self.messagesModel = MessagesModel()
        self.ui.messagesView.setModel(self.messagesModel)
        self.checkTimer = QTimer(self)
        self.checkTimer.timeout.connect(self.checkTimer_timeout)
        self.checkTimer_timeout()
        self.checkTimer.start(settings['updateTimeout'])

    def checkTimer_timeout(self):
        self.messagesModel.setMailHeaders(self.currentAccount().getHeaders())

    @pyqtSlot()
    def on_aboutQtAction_triggered(self):
        QMessageBox.aboutQt(self)

    @pyqtSlot()
    def on_aboutAction_triggered(self):
        QMessageBox.information(self, self.tr("About POPeek"), self.tr("POPeek by Matteo Italia\n\nReleased under MIT License"))

    def currentAccount(self):
        return accounts[0]

    def updateMessages(self):
        h=self.currentAccount().getHeaders()

def main():
    execfile('popeek.init', settings)
    global accounts
    accounts = settings['accounts']
    app = QApplication(sys.argv)
    m = MainWindow()
    m.show()
    app.exec_()

main()
