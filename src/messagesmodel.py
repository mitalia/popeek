from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MessagesModel(QAbstractTableModel):
    headers = ['Date', 'From', 'To', 'Subject']

    def __init__(self):
        QAbstractTableModel.__init__(self)
        self.mailHeaders = []

    def setMailHeaders(self, mailHeaders):
        self.beginResetModel()
        self.mailHeaders = mailHeaders
        self.endResetModel()

    def headerData(self, section, orientation, role):
        if role!=Qt.DisplayRole or orientation != Qt.Horizontal or section < 0 or section > len(self.headers):
            return QVariant()
        return self.headers[section]

    def data(self, index, role):
        #index = QModelIndex(index)
        if role!=Qt.DisplayRole:
            return QVariant()
        r, c = index.row(), index.column()
        mh=self.mailHeaders
        if not (0<=r<len(mh) and 0<=c<len(self.headers)):
            return QVariant()
        d = self.mailHeaders[r].msg
        if not self.headers[c] in d:
            return "(unknown)"
        return d[self.headers[c]]

    def rowCount(self, parent):
        if parent.isValid():
            return 0
        return len(self.mailHeaders)

    def columnCount(self, parent):
        if parent.isValid():
            return 0
        return len(self.headers)
