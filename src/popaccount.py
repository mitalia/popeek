# -*- coding: utf-8 -*-
from poplib import POP3
from email.parser import Parser
from email.message import Message

class MailHeaders:
    def __init__(self, idx, topRows):
        self.source = '\r\n'.join(topRows)
        p = Parser()
        self.msg = p.parsestr(self.source, True)

class POPAccount:
    def __init__(self, host, port, user, password):
        self.host=host
        self.port=port
        self.user=user
        self.password=password

    def getHeaders(self):
        p = POP3(self.host, self.port)
        p.user(self.user)
        p.pass_(self.password)
        headers = []
        for s in p.list()[1]:
            mid = int(s.split(' ')[0])
            headers.append(MailHeaders(mid, p.top(mid, 0)[1]))
        p.quit()
        return headers

